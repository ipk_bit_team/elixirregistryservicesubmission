 ELIXIR Registry Service Submission
 Copyright (c) by Daniel Arend, IPK-Gatersleben, Germany
 -------------------------------------------------------------------------------
 
 This projects may be used as template to embed the update or registration of
 a bio.tools registry into a (Maven) software deployment process. The authors apply
 this to update the e!DAL service. (https://bitbucket.org/edal_ipk/edal-project)
 
 The ELIXIR Registry Service Submission comprise JAVA class to apply the API of biotools registry and push a service
 description as XML file. This file comprise a dynamic part with variables
 (e.g. version, contributer etc.) that can be filled by the build process and
 a static part (e.g. service name, institute etc.) that is unlikely to be changed
 in every deployment process.
 
 The JAVA class (ElixirServiceRegistrySubmission.java) requires biotools login
 credential and those dynamic attributes as parameter and fill the XML file.
 Details, like method call and extension for custom dynamic element for a service
 description are documented in the JAVA source code.
 The static part must be adjusted in the XML file (tool.xml).
 
 If you embed this class into your Maven build process please bind it as 
 ant task into your POM like shown in the example POM.
 -------------------------------------------------------------------------------
 Quick Start
 
 - bind ElixirServiceRegistrySubmission Java class in bind to [site] phase of Maven build
 - run via commandline "mvn compile site"
 - parameters can be set in the Maven pom.xml in the entry of the "maven-antrun-plugin"
 - the properties of the resource that should be registered can be set in the tool.xml (see https://bio.tools/schema)