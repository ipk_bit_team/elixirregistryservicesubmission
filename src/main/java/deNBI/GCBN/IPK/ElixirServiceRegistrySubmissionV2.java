/*
 * Copyright (c) 2017 Leibniz Institute of Plant Genetics and Crop Plant Research (IPK), Gatersleben, Germany.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Creative Commons Attribution-NoDerivatives 4.0 International (CC BY-SA 4.0)
 * which accompanies this distribution, and is available at http://creativecommons.org/licenses/by-sa/4.0/
 *
 * Contributors:
 *      Leibniz Institute of Plant Genetics and Crop Plant Research (IPK), Gatersleben, Germany
 */
package deNBI.GCBN.IPK;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import org.apache.http.HttpHeaders;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

public class ElixirServiceRegistrySubmissionV2 {

	private static final String CODING_UTF_8 = "UTF-8";
	private static final String PATH_TO_JSON = "deNBI/GCBN/IPK/tool.json";

	private String user, password, version = null;
	private boolean overrideExistingEntry = false;

	/**
	 * 
	 * Default constructor to load all VeloCity and System properties.
	 * 
	 * @param version
	 * @param user
	 * @param password
	 */
	ElixirServiceRegistrySubmissionV2(boolean overrideExistingEntry, String version, String user, String password) {

		this.overrideExistingEntry = overrideExistingEntry;
		this.version = version;
		this.user = user;
		this.password = password;

		Velocity.setProperty("resource.loader", "class");
		Velocity.setProperty("class.resource.loader.class",
				"org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		Velocity.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.NullLogSystem");
		Velocity.setProperty("input.encoding", "UTF-8");
		Velocity.setProperty("output.encoding", "UTF-8");
		Velocity.init();

		// System.setProperty("http.proxyHost", "proxy");
		// System.setProperty("http.proxyPort", "1234");
		// System.setProperty("https.proxyHost", "proxy");
		// System.setProperty("https.proxyPort", "1234");
		// System.setProperty("java.net.useSystemProxies", "true");
	}

	/**
	 * Function to create XML for POSt to ELIXIR and validate it against XSD Schema
	 * file
	 * 
	 * @return {@link StringWriter} XML output
	 */
	private StringWriter createJSON() {

		StringWriter output = new StringWriter();

		try {
			VelocityContext context = new VelocityContext();

			context.put("version", this.version);
			Velocity.mergeTemplate(PATH_TO_JSON, CODING_UTF_8, context, output);
			output.flush();
			output.close();

		} catch (IOException e) {
			System.out.println("Creation of JSON failed: \n" + e);
			return null;
		}

		return output;

	}

	private void registerToElixir(StringWriter output) {

		Client client = Client.create();

		WebResource resource = client.resource("https://bio.tools/api/rest-auth/login/");

		Map<String, String> map = new HashMap<String, String>();
		map.put("password", this.password);
		map.put("username", this.user);

		JSONObject userValues = new JSONObject(map);

		final ClientResponse responseForToken = resource.accept(MediaType.APPLICATION_JSON)
				.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, userValues.toJSONString());

		if (responseForToken.getStatus() == 200) {

			JSONObject jsonObject = null;
			try {
				jsonObject = (JSONObject) new JSONParser().parse((String) responseForToken.getEntity(String.class));
			} catch (ClientHandlerException | UniformInterfaceException | ParseException e) {
				System.out.println("Parsing of user token failed: " + e);
			}

			if (jsonObject.containsKey("key")) {
				String token = (String) jsonObject.get("key");

				resource = client.resource("https://bio.tools/api/tool/");

				final ClientResponse responseForXMLPost = resource.header(HttpHeaders.AUTHORIZATION, "Token " + token)
						.type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
						.post(ClientResponse.class, output.toString());

				System.out.println(responseForXMLPost);
			}
		} else {
			System.out.println("Authentication to ELIXIR failed: " + responseForToken.getStatusInfo());
		}
	}

	private void deleteExistingResourceEntry(String json) {

		JSONObject jsonDoc = null;
		try {
			jsonDoc = (JSONObject) new JSONParser().parse((String) json);
		} catch (ClientHandlerException | UniformInterfaceException | ParseException e) {
			System.out.println("Parsing of resource name failed: " + e);
		}

		String resourceName = (String) jsonDoc.get("id");

		Client client = Client.create();

		WebResource resource = client.resource("https://bio.tools/api/rest-auth/login/");

		Map<String, String> map = new HashMap<String, String>();
		map.put("password", this.password);
		map.put("username", this.user);

		JSONObject userValues = new JSONObject(map);

		final ClientResponse responseForToken = resource.accept(MediaType.APPLICATION_JSON)
				.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, userValues.toJSONString());

		if (responseForToken.getStatus() == 200) {

			JSONObject jsonObject = null;
			try {
				jsonObject = (JSONObject) new JSONParser().parse((String) responseForToken.getEntity(String.class));
			} catch (ClientHandlerException | UniformInterfaceException | ParseException e) {
				System.out.println("Parsing of user token failed: " + e);
			}

			if (jsonObject.containsKey("key")) {
				String token = (String) jsonObject.get("key");

				/**
				 * Removed, because it seams that the REST API do not longer support deleting a
				 * specific version. So therefore the function will delete the whole resource
				 **/

				// String getInformationURL = "https://bio.tools/api/tool/" + resourceName;
				//
				// resource = client.resource(getInformationURL);
				//
				// final ClientResponse responseForInformationGet = resource
				// .header(HttpHeaders.AUTHORIZATION, "Token " +
				// token).accept(MediaType.APPLICATION_JSON)
				// .type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
				//
				// JSONObject resourceInformation = null;
				// try {
				// resourceInformation = (JSONObject) new JSONParser()
				// .parse((String) responseForInformationGet.getEntity(String.class));
				// } catch (ClientHandlerException | UniformInterfaceException | ParseException
				// e) {
				// System.out.println("Parsing of resource information failed: " + e);
				// }
				//
				// String resourceVersion = (String) resourceInformation.get("version");
				//
				// String url = "https://bio.tools/api/tool/" + resourceName + "/version/" +
				// resourceVersion;

				String url = "https://bio.tools/api/tool/" + resourceName;

				resource = client.resource(url.replaceAll(" ", "%20"));

				final ClientResponse responseForXMLPost = resource.header(HttpHeaders.AUTHORIZATION, "Token " + token)
						.delete(ClientResponse.class);

				System.out.println(responseForXMLPost);
			}
		} else {
			System.out.println("Authentication to ELIXIR failed: " + responseForToken.getStatusInfo());
		}
	}

	private boolean isOverrideExistingEntry() {
		return overrideExistingEntry;
	}

	public static void main(String[] args) throws InterruptedException {

		ElixirServiceRegistrySubmissionV2 submission = new ElixirServiceRegistrySubmissionV2(
				Boolean.parseBoolean(args[0]), args[1], args[2], args[3]);

		StringWriter jsonOutput = submission.createJSON();

		if (jsonOutput != null) {

			if (submission.isOverrideExistingEntry()) {

				submission.deleteExistingResourceEntry(jsonOutput.toString());
			}
			// Thread.sleep(2000);
			submission.registerToElixir(jsonOutput);

			// submission.postToElixir(jsonOutput);
		}

	}

}
