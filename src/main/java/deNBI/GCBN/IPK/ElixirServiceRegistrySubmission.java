/*
 * Copyright (c) 2017 Leibniz Institute of Plant Genetics and Crop Plant Research (IPK), Gatersleben, Germany.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Creative Commons Attribution-NoDerivatives 4.0 International (CC BY-SA 4.0)
 * which accompanies this distribution, and is available at http://creativecommons.org/licenses/by-sa/4.0/
 *
 * Contributors:
 *      Leibniz Institute of Plant Genetics and Crop Plant Research (IPK), Gatersleben, Germany
 */
package deNBI.GCBN.IPK;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

@Deprecated
public class ElixirServiceRegistrySubmission {

	private static final String CODING_UTF_8 = "UTF-8";
	private static final String PATH_TO_XML = "deNBI/GCBN/IPK/tool.xml";
	private String user, password, version = null;
	private boolean overrideExistingEntry = false;

	/**
	 * 
	 * Default constructor to load all VeloCity and System properties.
	 * 
	 * @param version
	 * @param user
	 * @param password
	 */
	ElixirServiceRegistrySubmission(boolean overrideExistingEntry, String version, String user, String password) {

		this.overrideExistingEntry = overrideExistingEntry;
		this.version = version;
		this.user = user;
		this.password = password;

		Velocity.setProperty("resource.loader", "class");
		Velocity.setProperty("class.resource.loader.class",
				"org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		Velocity.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.NullLogSystem");
		Velocity.setProperty("input.encoding", "UTF-8");
		Velocity.setProperty("output.encoding", "UTF-8");
		Velocity.init();

		// System.setProperty("http.proxyHost", "proxy");
		// System.setProperty("http.proxyPort", "1234");
		// System.setProperty("https.proxyHost", "proxy");
		// System.setProperty("https.proxyPort", "1234");
		// System.setProperty("java.net.useSystemProxies", "true");
	}

	/**
	 * Function to create XML for POSt to ELIXIR and validate it against XSD
	 * Schema file
	 * 
	 * @return {@link StringWriter} XML output
	 */
	private StringWriter createAndValidateXML() {

		StringWriter output = new StringWriter();

		try {
			VelocityContext context = new VelocityContext();

			context.put("version", this.version);
			Velocity.mergeTemplate(PATH_TO_XML, CODING_UTF_8, context, output);
			output.flush();
			output.close();

			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

			Schema schema = schemaFactory.newSchema(
					new StreamSource(ElixirServiceRegistrySubmission.class.getResourceAsStream("biotools-1.4.xsd")));

			Validator validator = schema.newValidator();

			InputStream inputStream = new ByteArrayInputStream(output.toString().getBytes(StandardCharsets.UTF_8));

			validator.validate(new StreamSource(inputStream));

		} catch (SAXException | IOException e) {
			System.out.println("Validation of your XML schema failed: \n" + e);
			return null;
		}

		return output;

	}

	private void postToElixir(StringWriter output) {

		Client client = Client.create();

		WebResource resource = client.resource("https://bio.tools/api/auth/login");

		Map<String, String> map = new HashMap<String, String>();
		map.put("password", this.password);
		map.put("username", this.user);

		JSONObject userValues = new JSONObject(map);

		final ClientResponse responseForToken = resource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,
				userValues.toJSONString());

		if (responseForToken.getStatus() == 200) {

			JSONObject jsonObject = null;
			try {
				jsonObject = (JSONObject) new JSONParser().parse((String) responseForToken.getEntity(String.class));
			} catch (ClientHandlerException | UniformInterfaceException | ParseException e) {
				System.out.println("Parsing of user token failed: " + e);
			}

			if (jsonObject.containsKey("token")) {
				String token = (String) jsonObject.get("token");

				resource = client.resource("https://bio.tools/api/tool");

				final ClientResponse responseForXMLPost = resource.accept(MediaType.APPLICATION_JSON)
						.header("Authorization", "Token " + token).type(MediaType.APPLICATION_XML)
						.post(ClientResponse.class, output.toString());

				System.out.println(responseForXMLPost);
			}
		} else {
			System.out.println("Authentication to ELIXIR failed: " + responseForToken.getStatusInfo());
		}
	}

	private void deleteExistingResourceEntry(String XML) {

		String resourceName = null;

		try {

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(XML)));

			NodeList list = doc.getElementsByTagName("name");

			if (list.getLength() == 1) {
				resourceName = list.item(0).getTextContent();
			} else {
				System.out.println("could not extract resource name");
			}
		} catch (ParserConfigurationException | SAXException | IOException e) {
			System.out.println("failed to parse resource name from XML");
		}

		Client client = Client.create();

		WebResource resource = client.resource("https://bio.tools/api/auth/login");

		Map<String, String> map = new HashMap<String, String>();
		map.put("password", this.password);
		map.put("username", this.user);

		JSONObject userValues = new JSONObject(map);

		final ClientResponse responseForToken = resource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,
				userValues.toJSONString());

		if (responseForToken.getStatus() == 200) {

			JSONObject jsonObject = null;
			try {
				jsonObject = (JSONObject) new JSONParser().parse((String) responseForToken.getEntity(String.class));
			} catch (ClientHandlerException | UniformInterfaceException | ParseException e) {
				System.out.println("Parsing of user token failed: " + e);
			}

			if (jsonObject.containsKey("token")) {
				String token = (String) jsonObject.get("token");

				String url = "https://bio.tools/api/tool/" + this.user + "/" + resourceName;

				resource = client.resource(url.replaceAll(" ", "%20"));

				final ClientResponse responseForXMLPost = resource.accept(MediaType.APPLICATION_JSON)
						.header("Authorization", "Token " + token).type(MediaType.APPLICATION_XML)
						.delete(ClientResponse.class);

				System.out.println(responseForXMLPost);
			}
		} else {
			System.out.println("Authentication to ELIXIR failed: " + responseForToken.getStatusInfo());
		}
	}

	private boolean isOverrideExistingEntry() {
		return overrideExistingEntry;
	}

	public static void main(String[] args) {

		ElixirServiceRegistrySubmission submission = new ElixirServiceRegistrySubmission(Boolean.parseBoolean(args[0]),
				args[1], args[2], args[3]);

		StringWriter xmlOutput = submission.createAndValidateXML();

		if (xmlOutput != null) {

			if (submission.isOverrideExistingEntry()) {

				submission.deleteExistingResourceEntry(xmlOutput.toString());
			}

			submission.postToElixir(xmlOutput);
		}
	}

}
